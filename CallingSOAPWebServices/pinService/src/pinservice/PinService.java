
package pinservice;

import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;

public class PinService {
    
    public static void main(String[] args) throws Exception {
        String pinId = "1100500631619";

       PinService ServicePIN = new PinService();
        ServicePIN.processRequest(pinId);
    }

    public void processRequest(String pinId) throws Exception {

        SOAPConnectionFactory soapCon =
                SOAPConnectionFactory.newInstance();
        SOAPConnection connection =
                soapCon.createConnection();
        MessageFactory mFac =
                MessageFactory.newInstance();

        SOAPMessage message =
                mFac.createMessage();

        SOAPHeader header = message.getSOAPHeader();
        SOAPBody body = message.getSOAPBody();
        header.detachNode();

        SOAPFactory soapFactory =
                SOAPFactory.newInstance();
         
        //add element ServicePIN into body
        SOAPBodyElement ServicePIN =
                body.addBodyElement(soapFactory.createName("ServicePIN",
                "ns", "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        
        //add username that are child element of ServicPIN
        SOAPElement username =
                ServicePIN.addChildElement(
                soapFactory.createName("username", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        username.addTextNode("anonymous");

       //add password that are child element of ServicPIN
        SOAPElement password =
                ServicePIN.addChildElement(
                soapFactory.createName("password", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        password.addTextNode("anonymous");

        //add PIN that are child element of ServicPIN
        //and the PIN also have to has String pinId that assigned by user
        SOAPElement pinElement =
                ServicePIN.addChildElement(
                soapFactory.createName("PIN", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        pinElement.addTextNode(pinId); 

        MimeHeaders h = message.getMimeHeaders();
        h.addHeader("SOAPAction",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService/ServicePIN");

        message.saveChanges();
        System.out.println("REQUEST:");
        displayMessage(message);

        System.out.println("\n");
        XTrustProvider.install();
        SOAPConnection conn =
                SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx");

        System.out.println("RESPONSE:");

        displayMessage(response);
    }

    private void displayMessage(SOAPMessage message) {
        try {
            TransformerFactory tFac = TransformerFactory.newInstance();
            Transformer transformer = tFac.newTransformer();
            Source src = message.getSOAPPart().getContent();
            StreamResult result = new StreamResult(System.out);
            transformer.transform(src, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}