<?php
header("content-type:text/xml");
// specify WSDL Web service that we want to call
$wsdl = 'http://webservicex.com/globalweather.asmx?WSDL';

// create SoapClient object
$client = new SoapClient($wsdl);


// initialize method name
$methodName = 'GetWeather';

// initialize method parameters
$params = array('CityName'=>'Khon Kaen','CountryName'=>'Thailand');

// initialize soapAction
$soapAction = 'http://www.webserviceX.NET';

// calling WS operation using soapCall
$objectResult = $client->__soapCall($methodName, array('parameters'=> $params), array('soapaction' => $soapAction));

// save the response
$response = $objectResult->GetWeatherResult;
$result = $client->GetWeather($params);

// display response
//echo $response;
$weatherXML = $result->GetWeatherResult;
echo $weatherXML;
?>

